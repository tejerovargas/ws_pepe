package com.salesianostriana.proyecto.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.salesianostriana.proyecto.model.LineaPedido;

@Repository
public interface LineaPedRepository extends JpaRepository<LineaPedido, Long> {

}

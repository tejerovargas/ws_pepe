package com.salesianostriana.proyecto.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.salesianostriana.proyecto.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

}

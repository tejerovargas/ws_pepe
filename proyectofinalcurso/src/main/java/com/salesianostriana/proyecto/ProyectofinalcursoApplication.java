package com.salesianostriana.proyecto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.salesianostriana.proyecto.model.Categoria;
import com.salesianostriana.proyecto.model.Producto;
import com.salesianostriana.proyecto.model.Usuario;
import com.salesianostriana.proyecto.repo.CategoriaRepository;
import com.salesianostriana.proyecto.repo.ProductoRepository;
import com.salesianostriana.proyecto.repo.UsuarioRepository;

@SpringBootApplication
public class ProyectofinalcursoApplication {

	@Autowired
	UsuarioRepository usuarioRepo;

	@Autowired
	ProductoRepository productoRepo;

	@Autowired
	CategoriaRepository categoriaRepo;

	public static void main(String[] args) {
		SpringApplication.run(ProyectofinalcursoApplication.class, args);
	}

	// CON ESTE METODO LO QUE HACE ES, AL INICIAR EL PROYECTO, EJECUTA TRAS MONTAR
	// LA BD Y DEMAS ESTAS SENTENCIAS. POR ELLO, HE CREADO ESTOS DATOS DE PRUEBA
	// PARA QUE SI LA BD NO ESTUVIERA CREADA O NO TUVIERA DATOS PUES QUE SE PUEDAN
	// VISUALIZAR ALGUNOS DATOS DE PRUEBA. CUANDO ENTREGUES EL PROYECTYO ES
	// RECOMENDABLE TENER ALGUNOS DATOS MAS Y TENER LA BASE DE DATOS BORRADA Y EN
	// CREATE

	@Bean
	public CommandLineRunner init() {
		return (args) -> {

			usuarioRepo.save(new Usuario("Pepe", "Leal", "35", "pepe@pepe.com", "1234", "pepito", true));
			usuarioRepo.save(new Usuario("Mario", "Oreja", "12", "mario@pepe.com", "1234", "elOreja", true));
			usuarioRepo.save(new Usuario("Juan", "Campos", "23", "jc@pepe.com", "1234", "juanishou", false));
			usuarioRepo.save(new Usuario("Miguel", "Campos", "60", "miguel.campos@pepe.com", "1234", "migueli", false));

			categoriaRepo.save(new Categoria("Sudadera"));
			categoriaRepo.save(new Categoria("Pantalon"));

			productoRepo.save(new Producto("Roler Black", "Camiseta deportiva para todos los deportes", 22,
					"La bellota", "Rojo", "XL", categoriaRepo.save(new Categoria("Camiseta"))));

		};
	}
}

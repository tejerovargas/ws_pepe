package com.salesianostriana.proyecto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app") // ESTA ANOTACION LO QUE ESTA DICIENDO ES... QUIERO QUE TODOS LOS METODOS DE
						// ESTE CONTROLADOR RESPONDAN A LA RUTA localhost:9000/app/'el metodo que sea'.
						// ES UTIL A LA HORA DE CONTROLAR LA SEGURIDAD DE NUESTRO PROYECTO, POR EJEMPLO.
						// SI NO HAY NINGUN USUARIO LOGUEADO, QUIERO QUE SI INTENTA ENTRAR EN ALGUNA
						// RUTA DE localhost:9000/app LO MANDE DIRECTAMENTE AL LOGIN
public class InicioController {

	// SOBRE LOS CONTROLADORES: SE RECOMIENDA TENER UN CONTROLADOR POR CADA CLASE
	// POJO DE NUESTRO PROYECTO, Y QUE EN CADA UNA SE GESTIONE Y MUESTRE LAS
	// PLANTILLAS RELACIONADAS CON CADA POJO. TAMBIEN ES RECOMENDABLE QUE HAYA OTRO
	// CONTROLADOR PARA EL LOGIN Y HACER LAS COMPROBACIONES PERTINENTES

	// EL MECANISMO DE LA APLICACION TIENE QUE SER LA SIGUIENTE.
	// Vista con formulario -> Controlador -> Service -> Repository y cuando
	// obtengas una respuesta se realizará el 'return' del controller
	@GetMapping({ "/", "/Inicio" })
	public String Inicio() {
		return "Inicio";
	}

}

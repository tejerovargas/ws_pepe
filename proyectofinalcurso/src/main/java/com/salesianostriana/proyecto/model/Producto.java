package com.salesianostriana.proyecto.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nombre;
	private String descripcion;
	private double precio;
	private String marca;
	private String color;
	private String talla;
	// LA EL ATRIBUTO DE 'CANTIDAD' DEBE ESTAR EN LINEA DE PEDIDO O EN PEDIDOS YA
	// QUE UN PRODUCTO(SUDADERA) NO TIENE DE POR SI UNA CANTIDAD, SINO QUE ES EN EL
	// PEDIDO DONDE SE SEÑALA CUANTAS QUIERE EL USUARIO
	// private int cantidad;

	// UN PRODUCTO 'CAMISETA ROYAL BULLS' PERTENECE A UNA SOLA CATEGORIA, POR LO
	// TANTO LA ANOTACION NECESARIA ES 'MANYTOONE'
	@ManyToOne
	private Categoria categoria;

	@OneToMany(mappedBy = "producto")
	private List<LineaPedido> lineaPedido;

	// Constructores
	public Producto() {
		super();
	}

	public Producto(String nombre, String descripcion, double precio, String marca, String color, String talla,
			Categoria categoria) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.marca = marca;
		this.color = color;
		this.talla = talla;
		this.categoria = categoria;
	}

	// Getter and setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}

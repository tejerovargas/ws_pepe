package com.salesianostriana.proyecto.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Categoria {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// SOLO NECESITAS ESTE ATRIBUTO, DADO QUE EN EL ALMACENARAS LOS TIPOS DE
	// PRODUCTOS QUE TENGAS (CAMISETA, SUDADERA O LO QUE QUIERAS TENER)
	private String tipo;

	// UNA CATEGORIA (SUDADERA) TIENE MUCHOS PRODUCTOS, LO CUAL LA ANOTACION
	// CORRECTA ES 'ONETOMANY'
	// SE HA DE PONER LA ANOTACION MAPPED PARA DECIRLE AL PROGRAMA A QUE ATRIBUTO DE
	// LA CLASE 'PRODUCTOS' ESTAMOS HACIENDO REFERENCIA
	@OneToMany(mappedBy = "categoria")
	private List<Producto> producto;

	// Constructores (SIEMPRE NECESITAS UNO VACIO PARA QUE SE PUEDA CREAR BIEN LOS
	// MODELOS EN LA BD)
	public Categoria(String tipo) {
		this.tipo = tipo;
	}

	public Categoria() {
		super();
	}

	// Getter and Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Producto> getProducto() {
		return producto;
	}

	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}

}

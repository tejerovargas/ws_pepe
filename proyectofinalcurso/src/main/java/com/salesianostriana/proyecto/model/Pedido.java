package com.salesianostriana.proyecto.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pedido {

	@Id
	@GeneratedValue
	private Long id;
	private LocalDateTime fecha;
	@OneToMany(mappedBy = "pedido")
	private List<LineaPedido> lineaPedido;
	private Double precioFinal;

	// Constructores
	public Pedido() {
		super();
	}

	public Pedido(Long id, LocalDateTime fecha, List<LineaPedido> lineaPedido, Double precioFinal) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.lineaPedido = lineaPedido;
		this.precioFinal = precioFinal;
	}

	// Getter and Setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public List<LineaPedido> getLineaPedido() {
		return lineaPedido;
	}

	public void setLineaPedido(List<LineaPedido> lineaPedido) {
		this.lineaPedido = lineaPedido;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrecioFinal() {
		return precioFinal;
	}

	public void setPrecioFinal(Double precioFinal) {
		this.precioFinal = precioFinal;
	}

}
